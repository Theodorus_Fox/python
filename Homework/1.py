import re

string = input()
s = re.sub(r'(\b\w+)((\s+\1)+)', lambda x: x.group(1) + re.sub(x.group(1), '<b>'+x.group(1)+'</b>', x.group(2)), string)

print(s)
