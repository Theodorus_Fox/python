import re

string = input()
s = re.fullmatch(r'('
                 '('
                 '(\d{1,3})'
                 '('
                 '(\s(\d{3}))*'
                 '((,|\.)\d+)?)'
                 ')'
                 '|'
                 '('
                 '(\d{1,3})'
                 '('
                 '(,(\d{3})+)*'
                 '(\.\d+)?)'
                 ')'
                 ')$', string)

print(s)