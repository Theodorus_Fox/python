#Оператор yield
import itertools #готовые итераторы


def fib(n):
    a, b, = 1, 1
    for i in range(n):
        yield a             #выдаёт поочеродно новые значения а через print(next(fib(n))
        a, b = b, a + b

class Fib:
    def __init__(self):
        self.a, self.b = 1, 1

    def __iter__(self):
        return self

    def __next__(self):
        c = self.a
        self.a, self.b = self.b, self.a + self.b
        return c

for index, obj in enumerate(fib(15)):       #нумерует вызов fib
    print(index+1, obj)

seq = itertools.islice(Fib(), 6)

for i in itertools.combinations(seq, 3):
    print(i)
