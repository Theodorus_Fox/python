# интсрумент работы со строчками

# [ab56], [a-z], [a\]b], [a-zA-Z]
# [^мн-во символов] - символы не из множества

# \d = [0-9]
# \s - любой пробельный символ
# \S - люб. непроб. символ
# \w - [a-zA-Z0-9_]
# . - любой символ

# RE* - замыкание Клини конкатанация слов из массова RE (в том числе пустое мн-во)
# \d+ = \d\d*
# R? - 0, 1 repeat
# R{n} - n repeats
# R{n, m} - from n to m repeats
# R{n,} - >= n
# R{,n} - <= n
# R~(R)
# R|T - объединение  языков, задав R и T

# ^ - начало строки
# $ - конец строки
# "^\\\\$" - только \\



import re
print(bool(re.match("\\\\", "\\"))) # проверяет, есть ли первое во втором вначале. Применив bool, получим true
print(bool(re.fullmatch(".*(devil|hell|fuck|dick|motherfucker|Voland|hui|kurwa)*.", "He is a kurwa"))) # проверяет, есть ли первое во втором. Применив bool, получим true

re.fullmatch(".*devil*.", "He is a devil").groups() # вернёт все найденные слова в скобках
print(re.fullmatch(".*(devil|is).*", "He is a devil").groups()) # первое слово
print(re.findall("devil|suka", "devil suka"))
print(re.sub("devil|suka", "***", "devil blea suka")) #цензурируем